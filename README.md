# Run

* Run the following command in your terminal: `pkg-config python-3.9 --libs --cflags`. Modify `python-3.9` according to 
  your Python version.

* You should acquire some flags similar to `-I/usr/include/python3.9 -I/usr/include/x86_64-linux-gnu/python3.9`

* Add the following line in your `CMakeList.txt`: `set(CMAKE_CXX_FLAGS <python_flags>")` where `<python_flags>` is the output of the previous command.

* Compile and run with an IDE (or terminal) of your choice

