#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: uhf_band_gr_leo
# GNU Radio version: 3.8.2.0

from distutils.version import StrictVersion
import numpy as np

from tqdm import trange

def decode_rep(reps, img_path = "../resources/output.rgb", src_path="../resources/output_decoded.rgb"):
    enc_bits = np.unpackbits(np.fromfile(img_path, dtype = "uint8"))
    dec_bits = np.empty(len(enc_bits)//reps, dtype=object)

    height = len(enc_bits) // reps
    width = reps

    dec_bits = enc_bits[:height*width].reshape((height, width))

    dec_bits = dec_bits.sum(axis=1)

    dec_bits = dec_bits > reps // 2

    dec_bits = np.packbits(dec_bits)

    with open(src_path, "wb") as ffile:
        ffile.write(bytearray(dec_bits))
    return dec_bits



if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import blocks
import pmt
from gnuradio import digital
from gnuradio import fec
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import leo
import math
import satnogs

from gnuradio import qtgui

class uhf_band_gr_leo(gr.top_block, Qt.QWidget):

    def __init__(self, gs_gain, gs_pointing_error, sc_gain, sc_pointing_error, sc_elevation, ecc_rate, doppler_shift, atm_attenuation, rain_attenuation, free_space_path_loss,
                 tle_line1, tle_line2, start_epoch, end_epoch, reps, cfo=0, excess_bw=0.35, snr=12, sps=2):
        gr.top_block.__init__(self, "uhf_band_gr_leo")
        print("Repetitions:", reps)

        Qt.QWidget.__init__(self)
        self.setWindowTitle("uhf_band_gr_leo")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "uhf_band_gr_leo")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Parameters
        ##################################################
        self.cfo = cfo
        self.excess_bw = excess_bw
        self.snr = snr
        self.sps = sps

        self.free_space_path_loss_en = free_space_path_loss #1
        self.atmospheric_attenuation = atm_attenuation#1
        self.doppler_shift = doppler_shift#7
        self.rain_attenuation = rain_attenuation#3

        self.gs_antenna_gain = gs_gain
        self.gs_antenna_pointing_loss = gs_antenna_pointing_loss = 0
        self.sc_antenna_gain = sc_gain

        self.sc_antenna_pointing_loss = sc_antenna_pointing_loss = 0
        self.sc_elevation = 300

        ##################################################
        # Variables
        ##################################################
        self.Turnstile_Antenna = Turnstile_Antenna = leo.custom_antenna_make(3,436e6, 0, 10, self.sc_antenna_gain , 72, 0)
        self.Helical_Antenna = Helical_Antenna = leo.helix_antenna_make(1, 436e6,0, 0, self.gs_antenna_gain, 0.1725, 0.689)

        self.AcubeSAT = AcubeSAT = leo.satellite_make('TLE_1', tle_line1[:69], tle_line2[:69], 436e6, 436e6, 30, Turnstile_Antenna, Turnstile_Antenna, .4, 315, 20000)
        self.nfilts = nfilts = 64
        print(tle_line1)
        print(tle_line2)
        print(start_epoch)
        print(end_epoch)
        self.AUTH_GS = AUTH_GS = leo.tracker_make(AcubeSAT, 22.959887, 40.627233, 0.056, start_epoch,
                                                  end_epoch, 2**20, 436e6, 436e6, 41.14, Helical_Antenna, Helical_Antenna, 0.4, 245, 20000)

        self.variable_repetition_encoder_def_0 = variable_repetition_encoder_def_0 = fec.repetition_encoder_make(64, reps)
        self.variable_repetition_decoder_def_1 = variable_repetition_decoder_def_1 = fec.repetition_decoder.make(64, reps, 0.5)
        self.variable_ieee802_15_4_variant_decoder_0 = variable_ieee802_15_4_variant_decoder_0 = satnogs.ieee802_15_4_variant_decoder_make([0x55]*12, 14, [79, 121, 97], 5, satnogs.crc.CRC32_C, satnogs.whitening.make_ccsds(True), True, 328, False, False)
        self.variable_ieee802_15_4_encoder_1 = variable_ieee802_15_4_encoder_1 = satnogs.ieee802_15_4_encoder_make(0x55, 12, [79, 121, 97], satnogs.crc.CRC32_C, satnogs.whitening.make_ccsds(True), True)
        self.samp_rate = samp_rate = 2**20
        self.rrc_taps = rrc_taps = firdes.root_raised_cosine(nfilts, nfilts, 1.0/float(sps), excess_bw, 11*sps*nfilts)
        self.packet_len = packet_len = "pcktpcktpckt"
        self.UHF_Band_LEO_Model = UHF_Band_LEO_Model = leo.leo_model_make(AUTH_GS, 1, 5*self.free_space_path_loss_en,
                                                                          6*self.free_space_path_loss_en, 7*self.doppler_shift,
                                                                          self.atmospheric_attenuation, 3*self.rain_attenuation,
                                                                          True, 7.5, 0, 25)

        ##################################################
        # Blocks
        ##################################################
        self.qtgui_waterfall_sink_x_0_0 = qtgui.waterfall_sink_c(
            1024, #size
            firdes.WIN_HAMMING, #wintype
            436e6, #fc
            samp_rate/2, #bw
            "Transmitted Signal", #name
            1 #number of inputs
        )
        self.qtgui_waterfall_sink_x_0_0.set_update_time(.1)
        self.qtgui_waterfall_sink_x_0_0.enable_grid(True)
        self.qtgui_waterfall_sink_x_0_0.enable_axis_labels(True)


        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        colors = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_waterfall_sink_x_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_waterfall_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_waterfall_sink_x_0_0.set_color_map(i, colors[i])
            self.qtgui_waterfall_sink_x_0_0.set_line_alpha(i, alphas[i])
        self.qtgui_waterfall_sink_x_0_0.set_line_label(0, "32123")

        self.qtgui_waterfall_sink_x_0_0.set_intensity_range(-140, 10)

        self._qtgui_waterfall_sink_x_0_0_win = sip.wrapinstance(self.qtgui_waterfall_sink_x_0_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_waterfall_sink_x_0_0_win)
        self.qtgui_waterfall_sink_x_0 = qtgui.waterfall_sink_c(
            1024*4, #size
            firdes.WIN_BLACKMAN_hARRIS, #wintype
            436e6, #fc
            samp_rate/2, #bw
            "Received Signal", #name
            1 #number of inputs
        )
        self.qtgui_waterfall_sink_x_0.set_update_time(0.1)
        self.qtgui_waterfall_sink_x_0.enable_grid(True)
        self.qtgui_waterfall_sink_x_0.enable_axis_labels(True)



        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        colors = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in range(1):
            if len(labels[i]) == 0:
                self.qtgui_waterfall_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_waterfall_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_waterfall_sink_x_0.set_color_map(i, colors[i])
            self.qtgui_waterfall_sink_x_0.set_line_alpha(i, alphas[i])

        self.qtgui_waterfall_sink_x_0.set_intensity_range(-140, 10)

        self._qtgui_waterfall_sink_x_0_win = sip.wrapinstance(self.qtgui_waterfall_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_waterfall_sink_x_0_win)
        self.fec_generic_encoder_0 = fec.encoder(variable_repetition_encoder_def_0, gr.sizeof_char, gr.sizeof_char)
        self.digital_gmsk_mod_0 = digital.gmsk_mod(
            samples_per_symbol=2,
            bt=0.35,
            verbose=False,
            log=False)
        self.digital_gmsk_demod_0 = digital.gmsk_demod(
            samples_per_symbol=2,
            gain_mu=0.175,
            mu=.5,
            omega_relative_limit=0.005,
            freq_error=0.0,
            verbose=False,log=False)
        self.blocks_doppler_comp_0 = satnogs.doppler_compensation(samp_rate, 436e6, 0, True)
        self.blocks_unpacked_to_packed_xx_1 = blocks.unpacked_to_packed_bb(1, gr.GR_MSB_FIRST)
        self.blocks_unpacked_to_packed_xx_0_0 = blocks.unpacked_to_packed_bb(1, gr.GR_MSB_FIRST)
        self.blocks_throttle_1 = blocks.throttle(gr.sizeof_char*1, samp_rate,True)
        self.blocks_stream_to_tagged_stream_0 = blocks.stream_to_tagged_stream(gr.sizeof_char, 1, 64, "packet_len")
        self.blocks_packed_to_unpacked_xx_0 = blocks.packed_to_unpacked_bb(1, gr.GR_MSB_FIRST)
        mult = (sc_gain/10 + gs_gain/10)*60 - gs_pointing_error*10/180 - sc_pointing_error*10/180 + (reps-3)*25 - atm_attenuation*2 - rain_attenuation*2 - doppler_shift*15
        self.blocks_multiply_const_vxx_1 = blocks.multiply_const_cc(max(mult, 1))
        self.blocks_file_source_0 = blocks.file_source(gr.sizeof_char*1, '../resources/camera.rgb', False, 0, 0)
        self.blocks_file_source_0.set_begin_tag(pmt.PMT_NIL)
        self.blocks_file_sink_1_0_0 = blocks.file_sink(gr.sizeof_char*1, '../resources/dbg_output.rgb', False)
        self.blocks_file_sink_1_0_0.set_unbuffered(True)
        self.blocks_file_sink_1 = blocks.file_sink(gr.sizeof_char*1, '../resources/output.rgb', False)
        self.blocks_file_sink_1.set_unbuffered(True)
        self.channel_model = leo.channel_model(samp_rate, UHF_Band_LEO_Model, 1)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.channel_model, 'doppler'), (self.blocks_doppler_comp_0, 'doppler'))
        self.connect((self.channel_model, 0), (self.blocks_doppler_comp_0, 0))
        self.connect((self.channel_model, 2), (self.blocks_doppler_comp_0, 1))
        self.connect((self.blocks_doppler_comp_0, 0), (self.digital_gmsk_demod_0, 0))
        self.connect((self.blocks_doppler_comp_0, 0), (self.qtgui_waterfall_sink_x_0, 0))
        self.connect((self.blocks_file_source_0, 0), (self.blocks_throttle_1, 0))
        self.connect((self.blocks_multiply_const_vxx_1, 0), (self.channel_model, 0))
        self.connect((self.blocks_packed_to_unpacked_xx_0, 0), (self.fec_generic_encoder_0, 0))
        self.connect((self.blocks_stream_to_tagged_stream_0, 0), (self.blocks_file_sink_1_0_0, 0))
        self.connect((self.blocks_stream_to_tagged_stream_0, 0), (self.blocks_packed_to_unpacked_xx_0, 0))
        self.connect((self.blocks_throttle_1, 0), (self.blocks_stream_to_tagged_stream_0, 0))
        self.connect((self.blocks_unpacked_to_packed_xx_0_0, 0), (self.blocks_file_sink_1, 0))
        self.connect((self.blocks_unpacked_to_packed_xx_1, 0), (self.digital_gmsk_mod_0, 0))
        self.connect((self.digital_gmsk_demod_0, 0), (self.blocks_unpacked_to_packed_xx_0_0, 0))
        self.connect((self.digital_gmsk_mod_0, 0), (self.blocks_multiply_const_vxx_1, 0))
        self.connect((self.digital_gmsk_mod_0, 0), (self.qtgui_waterfall_sink_x_0_0, 0))
        self.connect((self.fec_generic_encoder_0, 0), (self.blocks_unpacked_to_packed_xx_1, 0))


    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "uhf_band_gr_leo")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_cfo(self):
        return self.cfo

    def set_cfo(self, cfo):
        self.cfo = cfo

    def get_excess_bw(self):
        return self.excess_bw

    def set_excess_bw(self, excess_bw):
        self.excess_bw = excess_bw
        self.set_rrc_taps(firdes.root_raised_cosine(self.nfilts, self.nfilts, 1.0/float(self.sps), self.excess_bw, 11*self.sps*self.nfilts))

    def get_snr(self):
        return self.snr

    def set_snr(self, snr):
        self.snr = snr

    def get_sps(self):
        return self.sps

    def set_sps(self, sps):
        self.sps = sps
        self.set_rrc_taps(firdes.root_raised_cosine(self.nfilts, self.nfilts, 1.0/float(self.sps), self.excess_bw, 11*self.sps*self.nfilts))

    def get_Turnstile_Antenna(self):
        return self.Turnstile_Antenna

    def set_Turnstile_Antenna(self, Turnstile_Antenna):
        self.Turnstile_Antenna = Turnstile_Antenna
        self.Turnstile_Antenna.set_pointing_error(10)

    def get_Helical_Antenna(self):
        return self.Helical_Antenna

    def set_Helical_Antenna(self, Helical_Antenna):
        self.Helical_Antenna = Helical_Antenna
        self.Helical_Antenna.set_pointing_error(0)

    def get_AcubeSAT(self):
        return self.AcubeSAT

    def set_AcubeSAT(self, AcubeSAT):
        self.AcubeSAT = AcubeSAT

    def get_nfilts(self):
        return self.nfilts

    def set_nfilts(self, nfilts):
        self.nfilts = nfilts
        self.set_rrc_taps(firdes.root_raised_cosine(self.nfilts, self.nfilts, 1.0/float(self.sps), self.excess_bw, 11*self.sps*self.nfilts))

    def get_AUTH_GS(self):
        return self.AUTH_GS

    def set_AUTH_GS(self, AUTH_GS):
        self.AUTH_GS = AUTH_GS

    def get_variable_repetition_encoder_def_0(self):
        return self.variable_repetition_encoder_def_0

    def set_variable_repetition_encoder_def_0(self, variable_repetition_encoder_def_0):
        self.variable_repetition_encoder_def_0 = variable_repetition_encoder_def_0

    def get_variable_repetition_decoder_def_1(self):
        return self.variable_repetition_decoder_def_1

    def set_variable_repetition_decoder_def_1(self, variable_repetition_decoder_def_1):
        self.variable_repetition_decoder_def_1 = variable_repetition_decoder_def_1

    def get_variable_ieee802_15_4_variant_decoder_0(self):
        return self.variable_ieee802_15_4_variant_decoder_0

    def set_variable_ieee802_15_4_variant_decoder_0(self, variable_ieee802_15_4_variant_decoder_0):
        self.variable_ieee802_15_4_variant_decoder_0 = variable_ieee802_15_4_variant_decoder_0

    def get_variable_ieee802_15_4_encoder_1(self):
        return self.variable_ieee802_15_4_encoder_1

    def set_variable_ieee802_15_4_encoder_1(self, variable_ieee802_15_4_encoder_1):
        self.variable_ieee802_15_4_encoder_1 = variable_ieee802_15_4_encoder_1

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle_1.set_sample_rate(self.samp_rate)
        self.qtgui_waterfall_sink_x_0.set_frequency_range(0, self.samp_rate/200)
        self.qtgui_waterfall_sink_x_0_0.set_frequency_range(0, self.samp_rate/200)

    def get_rrc_taps(self):
        return self.rrc_taps

    def set_rrc_taps(self, rrc_taps):
        self.rrc_taps = rrc_taps

    def get_packet_len(self):
        return self.packet_len

    def set_packet_len(self, packet_len):
        self.packet_len = packet_len

    def get_UHF_Band_LEO_Model(self):
        return self.UHF_Band_LEO_Model

    def set_UHF_Band_LEO_Model(self, UHF_Band_LEO_Model):
        self.UHF_Band_LEO_Model = UHF_Band_LEO_Model




def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "--cfo", dest="cfo", type=eng_float, default="0.0",
        help="Set cfo [default=%(default)r]")
    parser.add_argument(
        "--excess-bw", dest="excess_bw", type=eng_float, default="350.0m",
        help="Set excess_bw [default=%(default)r]")
    parser.add_argument(
        "--snr", dest="snr", type=eng_float, default="12.0",
        help="Set snr [default=%(default)r]")
    parser.add_argument(
        "--sps", dest="sps", type=intx, default=2,
        help="Set samples per symbol [default=%(default)r]")
    return parser


def main(gs_gain, gs_pointing_error, sc_gain, sc_pointing_error, sc_elevation, ecc_rate, doppler_shift, atm_attenuation, rain_attenuation, free_space_path_loss,
         tle1, tle2, start_epoch, end_epoch, reps, top_block_cls=uhf_band_gr_leo, options=None):
    if options is None:
        options = argument_parser().parse_args()

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(gs_gain, gs_pointing_error, sc_gain, sc_pointing_error, sc_elevation, ecc_rate, doppler_shift, atm_attenuation, rain_attenuation, free_space_path_loss,
                       tle1, tle2, start_epoch, end_epoch, reps, cfo=options.cfo, excess_bw=options.excess_bw, snr=options.snr, sps=options.sps)

    tb.start()

    tb.show()

    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        decode_rep(reps)
        tb.stop()
        tb.wait()

    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()

def run_script(gs_gain, gs_pointing_error, sc_gain, sc_pointing_error, sc_elevation, ecc_rate, doppler_shift, atm_attenuation, rain_attenuation, free_space_path_loss,
               tle1, tle2, start_epoch, end_epoch, reps):
    # Test return value

    main(gs_gain, gs_pointing_error, sc_gain, sc_pointing_error, sc_elevation, ecc_rate, doppler_shift, atm_attenuation, rain_attenuation, free_space_path_loss, tle1, tle2,
         start_epoch, end_epoch, reps)
    return "raccoon"

