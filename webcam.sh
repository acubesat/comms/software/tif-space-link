#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

fswebcam -r 960x540 -D 0 $SCRIPT_DIR/resources/camera.png
convert $SCRIPT_DIR/resources/camera.png -resize 867x1300 $SCRIPT_DIR/resources/camera.rgb
