#include <iostream>
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <thread>
#include <fstream>
#include <sstream>
#include "imgui.h"
#include <math.h>
#include <Python.h>
#include <regex>

const char *glsl_version = "#version 130";

ImVec4 col_red= ImColor(198, 48, 6, 255);
ImVec4 col_yellow = ImColor(241, 213, 25, 255);
ImVec4 col_green = ImColor(45, 180, 61, 255);
ImVec4 col_dark_green = ImColor(41, 163, 55, 255);
ImVec4 col_slightly_darker_green = ImColor(33, 133, 44, 255);


inline void push_col(ImVec4 col){
    float h, s, v;
    float r, g, b;

    ImGui::ColorConvertRGBtoHSV(col.x, col.y, col.z, h, s, v);


    ImGui::ColorConvertHSVtoRGB(h*0.8, s, v*0.9, r, g, b);
    ImVec4 light_dark = ImVec4(r, g, b, 1.0);

    ImGui::ColorConvertHSVtoRGB(h*0.8, s, v*0.7, r, g, b);
    ImVec4 dark = ImVec4(r, g, b, 1.0);

    ImGui::PushStyleColor(ImGuiCol_FrameBg, col);
    ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, light_dark);
    ImGui::PushStyleColor(ImGuiCol_FrameBgActive, col);
}


ImVec4 lerp(ImVec4 col1, ImVec4 col2, float t){
    return ImVec4(
            col1.x*(1-t) + col2.x*t,
            col1.y*(1-t) + col2.y*t,
            col1.z*(1-t) + col2.z*t,
            col1.w*(1-t) + col2.w*t
            );
}


std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}



ImVec4 det_color(int min, int max, int val, bool rev = false){
    float t = float(val - min)/(max - min);
    t = rev ? 1 - t : t;

    if (t < 0.5){
        return lerp(col_red, col_yellow, 2*t);
    } else {
        return lerp(col_yellow, col_green, 2*(t - 0.5));
    }
}

std::string format_str(std::string s, int size){
    int len = s.size();
    if (len < size){
        return s + std::string(size - len, '0');
    } else {
        return s.substr(0, size);
    }
}

std::shared_ptr<std::string> readImage(std::string path) {
    std::ifstream fin(path, std::ios::in | std::ios::binary);
    std::ostringstream oss;
    oss << fin.rdbuf();

    auto output = std::make_shared<std::string>(oss.str());

    std::cerr << "The size of the loaded image is " << output->size() << std::endl;
    return output;
}

int main(int argc, char* argv[]) {
    // Setup window
    if (!glfwInit())
        return 1;
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHintString(GLFW_X11_CLASS_NAME, "ImGui");
    GLFWwindow *window = glfwCreateWindow(1920, 900, "SpaceLink", NULL, NULL);
    if (window == NULL) {
        return 1;
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync
    bool err = gl3wInit() != 0;
    if (err) {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGuiIO &imguiIo = ImGui::GetIO();
    (void) imguiIo;

    ImGui::StyleColorsDark();
//  ImGui::StyleColorsClassic();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    std::string directory = __FILE__;
    directory.erase(directory.end() - 9, directory.end());

    imguiIo.Fonts->AddFontFromFileTTF((directory + "/lib/imgui/misc/fonts/DroidSans.ttf").c_str(), 18.0f);



//    imguiIo.IniFilename = nullptr;

    bool show_demo_window = false;
    bool doppler_shift = false;
    bool atmospheric_attenuation = false;
    bool rain_attenuation = false;
    bool free_space_path_loss = false;

    int gs_antenna_gain = 2.5;
    int gs_antenna_pointing_error = 0;

    int sc_antenna_gain = 2.5;
    int sc_antenna_pointing_error = 0;
    int sc_elevation = 300;

    std::string inclination = "97.3759";
    std::string ltan = "266.589" ;
    std::string eccentricity = "0.00010";
    std::string perigee = "000.0000";
    std::string mean_anomaly = "000.000";
    std::string mean_motion = "15.2426";


    const char* ecc_rate[] = {"1/2", "1/4", "1/6" };
    static int ecc_item_current = 0;

    const char* orbit[] = {"AcubeSAT", "Hubble", "ISS", "LEDSAT"};
    static int orbit_current = 0;

    const char* orbital_elements[] = {"Eccentricity", "Inclination", "LTAN", "Argument of periapsis", "Mean anomaly", "Mean Motion"};
    const char* orbital_elements_units[] = {"-", "deg", "deg", "deg", "deg", "revolutions/day"};
    std::string* orbital_elements_vals[] = {&eccentricity, &inclination, &ltan, &perigee, &mean_anomaly, &mean_motion};
    int win_width;

    std::string tle_line_1 = "1 69696U 16025E   23183.00000000  .00002000  00000-0  49111-4 0  6969";

    std::string tle_line_2 = "2 69696  97.3759 266.5890 0001000 000.0000 000.0000 15.24261762696969";

    std::string start_epoch = "2023-07-03T10:10:12.783Z";
    std::string end_epoch = "2023-07-03T10:17:17.733Z";

    ImVec4 tmp_col;

    bool take_photo = false;
    bool run_exp = false;
    bool image_updated = false;

    ImVec4 clear_color = ImColor(35, 44, 59);
    char out_text[100] = "";

    char filename[] = "../flowgraph.py";
    FILE* fp;

    Py_Initialize();

    PyObject* sysPath = PySys_GetObject("path");
    std::string path(argv[0]);

   // std::string base = path.substr(0, path.find_last_of("/", 0));

    // stupid shortcut
    PyList_Append(sysPath, PyUnicode_FromString(path.substr(0, path.find_last_of('/', path.size() - 18)).c_str()));

    PyObject *pName = PyUnicode_FromString("flowgraph");
    PyObject *pModule = PyImport_Import(pName);
    PyObject *pDict = PyModule_GetDict(pModule);
    PyObject *pFunc = PyDict_GetItem(pDict, PyUnicode_FromString("run_script"));

    PyObject *pFuncdecode = PyDict_GetItem(pDict, PyUnicode_FromString("decode_rep"));

    ImU32 row_bg_color_dark = ImGui::GetColorU32( ImVec4(67.0/256, 58.0/256, 63.0/256, 1));
    ImU32 row_bg_color_light = ImGui::GetColorU32( ImVec4(0.15, 0.15, 0.15, 1));

    while (!glfwWindowShouldClose(window)) {
        try {
            glfwPollEvents();

            // Start the Dear ImGui frame
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

            if (show_demo_window) {
                ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiCond_FirstUseEver);
                ImGui::ShowDemoWindow();
            }
/*
            ImGui::Begin(
                    "Hello, world!");                          // Create a window called "Hello, world!" and append into it.

            ImGui::Text(
                    "This is some useful text.");               // Display some text (you can use a format strings too)
            ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state

            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate,
                        ImGui::GetIO().Framerate);
            ImGui::End();
*/


            ImGui::Begin(
                    "Channel Model");
            win_width = ImGui::GetWindowWidth();
            ImGui::SetNextItemWidth(55);
            ImGui::Combo("Error Correction Code Rate", &ecc_item_current, ecc_rate, IM_ARRAYSIZE(ecc_rate));
            ImGui::Checkbox("Doppler Shift", &doppler_shift);
            ImGui::SameLine(std::max(win_width - 400, 300));
            ImGui::Checkbox("Atmospheric Attenuation", &atmospheric_attenuation);
            ImGui::Checkbox("Rain Attenuation", &rain_attenuation);
            ImGui::SameLine(std::max(win_width - 400, 300));
            ImGui::Checkbox("Free Space Path Loss", &free_space_path_loss);
            ImGui::End();

            ImGui::Begin(
                    "Ground Station");

            tmp_col = det_color(1, 10, gs_antenna_gain);
            push_col(tmp_col);
            ImGui::SliderInt("GS Antenna Gain [dB]", &gs_antenna_gain, 1, 10);
            ImGui::PopStyleColor(3);

            tmp_col = det_color(0, 180, gs_antenna_pointing_error, true);
            push_col(tmp_col);
            ImGui::SliderInt("GS Antenna Pointing Error [deg]", &gs_antenna_pointing_error, 0, 180);
            ImGui::PopStyleColor(3);
            ImGui::End();

            ImGui::Begin(
                    "Spacecraft");
            tmp_col = det_color(1, 10, sc_antenna_gain);
            push_col(tmp_col);
            ImGui::SliderInt("SC Antenna Gain [dB]", &sc_antenna_gain, 1, 10);
            ImGui::PopStyleColor(3);

            tmp_col = det_color(0, 180, sc_antenna_pointing_error, true);
            push_col(tmp_col);
            ImGui::SliderInt("SC Antenna Pointing Error [deg]", &sc_antenna_pointing_error, 0, 180);
            ImGui::PopStyleColor(3);

            ImGui::End();

            ImGui::Begin(
                    "Mission"
                    );

            if (ImGui::Combo("Orbit", &orbit_current, orbit, IM_ARRAYSIZE(orbit))){
                std::string orbit_name;
                std::ifstream istream;
                istream.exceptions(std::ios::eofbit | std::ios::failbit | std::ios::badbit);


                switch(orbit_current){
                    case 0:
                        istream.open("../TLEs/AcubeSAT.tle");
                        start_epoch = "2023-07-03T21:09:00.078Z";
                        end_epoch = "2023-07-03T21:16:12.182Z";
                        break;
                    case 1:
                        istream.open("../TLEs/Hubble.tle");
                        start_epoch = "2023-07-03T20:07:53.721Z";
                        end_epoch = "2023-07-03T20:12:07.758Z";
                        break;
                    case 2:
                        istream.open("../TLEs/ISS.tle");
                        start_epoch = "2021-09-03T05:50:39.816Z";
                        end_epoch = "2021-09-03T05:57:24.853Z";
                        break;
                    case 3:
                        istream.open("../TLEs/LEDSAT.tle");
                        start_epoch = "2021-08-17T21:02:33.453Z";
                        end_epoch = "2021-08-17T21:09:39.373Z";
                        break;
                    default:
                        std::cerr << "Incorrect file" << std::endl;
                }

                std::regex rgx("^(\\d+) (\\d+)  ([\\d\\.]+) ([\\d\\.]+) (\\d+) ([\\d\\.]+) ([\\d\\.]+) ([\\d\\.]+)");
                std::smatch rgx_match;

                getline(istream, tle_line_1);
                getline(istream, tle_line_2);
                std::regex_search(tle_line_2, rgx_match, rgx);

                inclination = format_str(rgx_match[3], 7);
                ltan = format_str(rgx_match[4], 7);
                eccentricity = format_str("0."+ std::string(rgx_match[5]), 7);
                perigee = format_str(rgx_match[6], 7);
                mean_anomaly = format_str(rgx_match[7], 7);
                mean_motion = format_str(rgx_match[8], 7);
            }


            ImGui::BeginTable("Orbital Elements", 3);

            ImGui::TableNextRow();
            ImGui::TableSetColumnIndex(0);
            ImGui::Text("Parameter");
            ImGui::TableSetColumnIndex(1);
            ImGui::Text("Unit");
            ImGui::TableSetColumnIndex(2);
            ImGui::Text("Value");

            for (int row = 0; row < IM_ARRAYSIZE(orbital_elements); row++){
                ImGui::TableNextRow();

                if (row%2 == 0){
                    ImGui::TableSetBgColor(ImGuiTableBgTarget_RowBg0, row_bg_color_light);
                }
                ImGui::TableSetColumnIndex(0);
                ImGui::Text(orbital_elements[row]);
                ImGui::TableSetColumnIndex(1);
                ImGui::Text(orbital_elements_units[row]);
                ImGui::TableSetColumnIndex(2);
                ImGui::Text(orbital_elements_vals[row]->c_str());
            }

            ImGui::EndTable();

            ImGui::End();

            ImGui::Begin(
                    "Experiment"
                    );

            win_width = ImGui::GetWindowWidth();
            ImGui::NewLine();
            ImGui::SameLine((win_width - 100)/2);
            ImGui::PushStyleColor(ImGuiCol_Button, col_green);
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, col_dark_green);
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, col_slightly_darker_green);
            take_photo = ImGui::Button("Take photo", ImVec2(100, 40));
            ImGui::PopStyleColor(3);
            ImGui::Dummy(ImVec2(0, 10));
            ImGui::NewLine();
            ImGui::SameLine((win_width - 100)/2);
            ImGui::PushStyleColor(ImGuiCol_Button, col_green);
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, col_dark_green);
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, col_slightly_darker_green);
            run_exp = ImGui::Button("RUN", ImVec2(100, 40));
            ImGui::PopStyleColor(3);

            if (take_photo) {
                exec("../webcam.sh");
                image_updated = true;
            }

            if (run_exp){
                PyObject* str_out = PyObject_CallFunction(pFunc, "iiiiisiiiissssi", gs_antenna_gain, gs_antenna_pointing_error,  sc_antenna_gain,
                                                          sc_antenna_pointing_error, sc_elevation,
                                                          ecc_rate[ecc_item_current], doppler_shift, atmospheric_attenuation, rain_attenuation,
                                                          free_space_path_loss, tle_line_1.c_str(), tle_line_2.c_str(),
                                                          start_epoch.c_str(), end_epoch.c_str(), ecc_item_current*2 + 3);
                const char* cnst_out_text = PyUnicode_AsUTF8(str_out);
                //PyObject_CallFunction(pFunc, "i", 3);
            }

            ImGui::End();

            ImGui::Begin("photo");

            static int width = 867;
            static int height = 1300;

            image_updated |= ImGui::SliderInt("width", &width, 8, 2560);
            image_updated |= ImGui::SliderInt("height", &height, 8, 2560);

            // Create a OpenGL texture identifier
            static bool imageLoaded = false;
            GLuint image_texture;

            static std::shared_ptr<std::string> imageData;

            if(run_exp || ImGui::Button("Reload image data") || image_updated) {
                imageData = readImage("../resources/output_decoded.rgb");
                image_updated = true;
            }

            if (image_updated && imageData && !imageData->empty()) {
                height = imageData->size() / (width * 3);

                if (imageData->size() < width * height * 3) {
                    std::cerr << "The image size " << imageData->size() << " is not large enough for your needs." << std::endl;
                    ImGui::Text("Image size %ld < %d", imageData->size(), width * height * 3);
                } else {
                    glGenTextures(1, &image_texture);
                    glBindTexture(GL_TEXTURE_2D, image_texture);

                    // Setup filtering parameters for display
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                                    GL_CLAMP_TO_EDGE); // This is required on WebGL for non power-of-two textures
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // Same

                    // Upload pixels into texture
#if defined(GL_UNPACK_ROW_LENGTH) && !defined(__EMSCRIPTEN__)
                    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
                    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
#endif
                    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE,
                                 imageData->c_str());

                    imageLoaded = true;
                }
            }



                if (imageLoaded) {
                ImGui::Image((void*)(intptr_t)image_texture, ImVec2(500, 500 * height/static_cast<float>(width)));
            }

            ImGui::End();

            image_updated = false;

            // Rendering
            ImGui::Render();
            int display_w, display_h;
            glfwGetFramebufferSize(window, &display_w, &display_h);
            glViewport(0, 0, display_w, display_h);
            glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w,
                         clear_color.w);
            glClear(GL_COLOR_BUFFER_BIT);
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            glfwSwapBuffers(window);
        } catch (const std::exception &e) {
            std::cerr << "Unhandled exception in main thread: " << e.what();
//            std::this_thread::sleep_for(50ms);
        } catch (...) {
            auto exception = std::current_exception();
            std::string explanation = typeid(exception).name();
            std::cerr << "Unhandled exception in main thread: " << explanation;

//            std::this_thread::sleep_for(50ms);
        }
    }
    Py_Finalize();
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();
}
